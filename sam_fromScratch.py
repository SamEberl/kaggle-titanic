import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder


def preprocess(X):
    #drop empty rows without 'Embarked' entry and reset index
    X.dropna(subset=['Embarked'], axis=0, inplace=True)
    X = X.reset_index(drop=True)
    #extract labels
    Y = X[['Survived']].to_numpy().T
    #drop unnecassary columns
    X = X.drop(['PassengerId', 'Name', 'Ticket', 'Cabin', 'Survived'], axis=1)
    #numericalize 'Sex' column
    mapping_Sex = {'male': 0, 'female': 1}
    X = X.replace({'Sex': mapping_Sex})
    #fill missing values in age
    X['Age'].fillna(value=X['Age'].mean(), inplace=True)
    #create one hot encoded matrix for 'Embarked'
    enc = OneHotEncoder(sparse=False)
    oh_array = enc.fit_transform(X[['Embarked']])
    #drop 'Embarked' column
    X.drop(['Embarked'], axis=1, inplace=True)
    #convert dataframe to numpy array
    X = X.to_numpy()
    #concatenate one hot encoded 'Embarked' to original data
    X = np.concatenate((X, oh_array), axis=1)
    #transpose X
    X = X.T
    return X, Y


def initialization(nbr_features):
    n1 = 4
    n2 = 1
    W1 = np.random.randn(n1, nbr_features) * np.sqrt(2/nbr_features)
    b1 = np.zeros((n1, 1))
    W2 = np.random.randn(n2, n1) * np.sqrt(2/n1)
    b2 = np.zeros((n2, 1))
    params = (W1, b1, W2, b2)
    # params = ({ 'W1': W1,
    #             'b1': b1,
    #             'W2': W2,
    #             'b2': b2})
    assert W1.shape == (n1, nbr_features)
    assert b1.shape == (n1, 1)
    assert W2.shape == (n2, n1)
    assert b2.shape == (n2, 1)
    return params


def relu(Z):
    Z = Z * (Z>0)
    return Z


def sigmoid(Z):
    Z = 1/(1 + np.exp(-Z))
    return Z


def forward_pass(X, params):
    W1, b1, W2, b2 = params
    A0 = X
    Z1 = W1 @ A0 + b1
    A1 = relu(Z1)
    Z2 = W2 @ A1 + b2
    A2 = sigmoid(Z2)
    # print(f'A0: {A0.shape}, W1:{W1.shape}')
    # print(f'A1: {A1.shape}, W2:{W2.shape}')
    cache =     {'A2': A2,
                 'A1': A1,
                 'W2': W2,
                 'Z1': Z1,
                 'X': X}
    return A2, cache


def calc_cost(Y, Y_hat):
    cost = np.sum((Y-Y_hat)**2)
    return cost

#backwardpass to difficult. Not gonna finish this
def backward_pass(cache, Y, Y_hat):
    A2 = cache['A2']
    A1 = cache['A1']
    W2 = cache['W2']
    Z1 = cache['Z1']
    X = cache['X']
    
    dA2 = 2*(Y-Y_hat)
    dZ2 = dA2 * A2 * (1-A2)
    dW2 = dZ2 * A1
    db2 = dZ2
    
    print(A1.shape, dZ2.shape, W2.shape)
    dA1 = dZ2 * W2
    dZ1 = dA1 * (Z1>0)
    dW1 = dZ1 * X
    db1 = dZ1

    grads = (dW1, db1, dW2, db2)
    return grads

x_raw = pd.read_csv('train.csv')
X, Y = preprocess(x_raw)
# X = X[:, 0].reshape(10, 1)
params = initialization(X.shape[0])
Y_hat, cache = forward_pass(X, params)
cost = calc_cost(Y, Y_hat)
grads = backward_pass(cache, Y, Y_hat)

print(Y_hat[:, 0:10])
print(cost)
print(f'grads: {grads}')